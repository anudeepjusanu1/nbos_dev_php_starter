'use strict';

/**
 * @ngdoc function
 * @name newappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the Entire Application
 */

angular.module('mod.config')
    .controller('AppCtrl', ['$scope', function ($scope) {


        /**
         *    NBOS NOTE: ENVIRONMENT VARIABLE IS SET HERE
         */

        $scope.message = "hello";

        $scope.APP_ENV = document.getElementById('ENV').innerHTML.toUpperCase();
        $scope.APP_SUBDOMAIN = document.getElementById('subdomain').innerHTML;

    }]);
