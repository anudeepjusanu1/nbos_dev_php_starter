'use strict';

/**
 * @ngdoc overview
 * @name CodingLabs
 * @description
 * # NBOS LAYOUT MODULE
 *
 * Main module of the application.
 */
angular
    .module('mod.layout', []);

angular
    .module('mod.layout')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

        $stateProvider
            .state('layout', {
                url: '',
                abstract : true,
                views : {
                    '':{
                        templateUrl: 'mod_config/layout/layout.html',
                        controller : 'LayoutCtrl'
                    },
                    'header@layout':{
                        templateUrl: 'mod_config/layout/header.html'
                    },
                    'nav@layout':{
                        templateUrl: 'mod_config/layout/nav.html'
                    },
                    'footer@layout':{
                        templateUrl: 'mod_config/layout/footer.html'
                    }
                }
            });

    }]);
