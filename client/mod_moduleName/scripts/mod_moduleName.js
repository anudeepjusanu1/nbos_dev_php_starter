angular.module('mod.m{{moduleId}}', []);

angular.module('mod.m{{moduleId}}')
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m{{moduleId}}', {
                url: '/{{moduleName}}',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m{{moduleId}}.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_{{moduleName}}/views/menu/{{moduleName}}.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "{{moduleName}}"
               }
           })
            .state('m{{moduleId}}.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_{{moduleName}}/views/dashboard.html",
                controller: function($scope) {
                    console.log("here");
                },
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "{{moduleName}}"
                }

            })

    }])