angular
    .module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'md.data.table',

        // Coding Labs
        'mod.config',
        'mod.layout',
        'mod.idn',
        'mod.nbos',
        'mod.console',
        'mod.dev',
        'mod.admin'
    ])
    .config(['localStorageServiceProvider', '$mdThemingProvider', function (localStorageServiceProvider, $mdThemingProvider) {

        //Angular Material Theme Configuration
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('deep-orange')
            .warnPalette('red');

        $mdThemingProvider.theme('docs-dark', 'default')
            .primaryPalette('yellow')
            .accentPalette('green')
            .warnPalette('red')
            .dark();

        //localstorage configuration
        localStorageServiceProvider
            .setPrefix('CODINGLABS');

    }]);